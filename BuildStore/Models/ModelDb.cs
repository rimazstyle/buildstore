﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BuildStore.Models
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options) { }
        public DbSet<Product> Products { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Caterogy> Caterogies { get; set; }
        public DbSet<SubCategory> SubCategories { get; set; }
        public DbSet<Storage> Storages { get; set; }
        public DbSet<Procurement> Procurements { get; set; }
        public DbSet<ProductIsAvalible> ProductIsAvalibles { get; set; }

    }

    public enum UnitsOfMeasurments
    {
        Piece,//штучно
        Volume,//объем
        Weight//вес
    }
    public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public UnitsOfMeasurments Unit { get; set; }

        public List<ProductIsAvalible> ProductIsAvalibles { get; set; }
        public decimal Price { get; set; }
        public List<Order> Orders { get; set; }
        public List<Procurement> Procurements { get; set; }

        public virtual SubCategory SubCaterogy { get; set; }
        public int? SubCaterogyId { get; set; }

        public Product()
        {
            ProductIsAvalibles = new List<ProductIsAvalible>();
            Orders = new List<Order>();
            Procurements = new List<Procurement>();
        }
    }
    public class Caterogy
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public List<SubCategory> SubCategories { get; set; }

        public Caterogy()
        {
            SubCategories = new List<SubCategory>();
        }
    }

    public class SubCategory
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int? CategoryId { get; set; }
        public virtual Caterogy Caterogy { get; set; }
        public int? MinimalAmountId { get; set; }
        public double Count { get; set; }

        public List<Product> Products { get; set; }
        public SubCategory()
        {
            Products = new List<Product>();
        }

    }

    public class Storage
    {
        public int Id { get; set; }
        public string Location { get; set; }
        public List<ProductIsAvalible> ProductIsAvalibles { get; set; }
        public List<Order> Orders { get; set; }
        public List<Procurement> Procurements { get; set; }
        //Добавить стилаж, полка, позиция (позиция необязательн)

        public Storage()
        {
            ProductIsAvalibles = new List<ProductIsAvalible>();
            Orders = new List<Order>();
            Procurements = new List<Procurement>();
        }
    }

    public class ProductIsAvalible //наличие продукта
    {
        public int Id { get; set; }
        public int? ProductId { get; set; }
        public virtual Product Product { get; set; }
        public int? StorageId { get; set; }
        public virtual Storage Storage { get; set; }
        public double Count { get; set; }

    }

    public class Procurement //закупки
    {
        public int Id { get; set; }
        public int? ProductId { get; set; }
        public virtual Product Product { get; set; }
        public int? StorageId { get; set; }
        public virtual Storage Storage { get; set; }
        public DateTime Date { get; set; }
        public decimal Amount { get; set; }
        public double Count { get; set; }
        public string ProviderName { get; set; } //Название поставщика


    }

    public class Order
    {
        public int Id { get; set; }
        public int? ProductId { get; set; }
        public virtual Product Product { get; set; }
        public int? StorageId { get; set; }
        public virtual Storage Storage { get; set; }
        public DateTime Date { get; set; }
        public decimal Amount { get; set; }
        public double Count { get; set; }

    }
}
