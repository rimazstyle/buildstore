﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BuildStore.Migrations
{
    public partial class Update3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MinimalAmounts");

            migrationBuilder.AddColumn<double>(
                name: "Count",
                table: "SubCategories",
                nullable: false,
                defaultValue: 0.0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Count",
                table: "SubCategories");

            migrationBuilder.CreateTable(
                name: "MinimalAmounts",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Count = table.Column<int>(nullable: false),
                    SubCategoryId = table.Column<int>(nullable: true),
                    SubCaterogyOf = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MinimalAmounts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MinimalAmounts_SubCategories_SubCaterogyOf",
                        column: x => x.SubCaterogyOf,
                        principalTable: "SubCategories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MinimalAmounts_SubCaterogyOf",
                table: "MinimalAmounts",
                column: "SubCaterogyOf",
                unique: true,
                filter: "[SubCaterogyOf] IS NOT NULL");
        }
    }
}
