﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BuildStore.Migrations
{
    public partial class Update2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "SubCaterogyId",
                table: "Products",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Products_SubCaterogyId",
                table: "Products",
                column: "SubCaterogyId");

            migrationBuilder.AddForeignKey(
                name: "FK_Products_SubCategories_SubCaterogyId",
                table: "Products",
                column: "SubCaterogyId",
                principalTable: "SubCategories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Products_SubCategories_SubCaterogyId",
                table: "Products");

            migrationBuilder.DropIndex(
                name: "IX_Products_SubCaterogyId",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "SubCaterogyId",
                table: "Products");
        }
    }
}
