﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using BuildStore.Models;

namespace BuildStore.Controllers
{
    public class CategoriesController : Controller
    {
        private readonly ApplicationDbContext _context;

        public CategoriesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Caterogies
        public async Task<IActionResult> Index()
        {
            return View(await _context.Caterogies.ToListAsync());
        }

        // GET: Caterogies/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var caterogy = await _context.Caterogies
                .FirstOrDefaultAsync(m => m.Id == id);
            if (caterogy == null)
            {
                return NotFound();
            }

            return View(caterogy);
        }

        // GET: Caterogies/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Caterogies/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,Description")] Caterogy caterogy)
        {
            if (ModelState.IsValid)
            {
                _context.Add(caterogy);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(caterogy);
        }

        // GET: Caterogies/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var caterogy = await _context.Caterogies.FindAsync(id);
            if (caterogy == null)
            {
                return NotFound();
            }
            return View(caterogy);
        }

        // POST: Caterogies/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,Description")] Caterogy caterogy)
        {
            if (id != caterogy.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(caterogy);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CaterogyExists(caterogy.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(caterogy);
        }

        // GET: Caterogies/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var caterogy = await _context.Caterogies
                .FirstOrDefaultAsync(m => m.Id == id);
            if (caterogy == null)
            {
                return NotFound();
            }

            return View(caterogy);
        }

        // POST: Caterogies/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var caterogy = await _context.Caterogies.FindAsync(id);
            _context.Caterogies.Remove(caterogy);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CaterogyExists(int id)
        {
            return _context.Caterogies.Any(e => e.Id == id);
        }
    }
}
