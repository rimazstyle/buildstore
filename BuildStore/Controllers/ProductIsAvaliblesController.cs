﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using BuildStore.Models;

namespace BuildStore.Controllers
{
    public class ProductIsAvaliblesController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ProductIsAvaliblesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: ProductIsAvalibles
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.ProductIsAvalibles.Include(p => p.Product).Include(p => p.Storage);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: ProductIsAvalibles/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var productIsAvalible = await _context.ProductIsAvalibles
                .Include(p => p.Product)
                .Include(p => p.Storage)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (productIsAvalible == null)
            {
                return NotFound();
            }

            return View(productIsAvalible);
        }

        // GET: ProductIsAvalibles/Create
        public IActionResult Create()
        {
            ViewData["ProductId"] = new SelectList(_context.Products, "Id", "Id");
            ViewData["StorageId"] = new SelectList(_context.Storages, "Id", "Id");
            return View();
        }

        // POST: ProductIsAvalibles/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,ProductId,StorageId,Count")] ProductIsAvalible productIsAvalible)
        {
            if (ModelState.IsValid)
            {
                _context.Add(productIsAvalible);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["ProductId"] = new SelectList(_context.Products, "Id", "Id", productIsAvalible.ProductId);
            ViewData["StorageId"] = new SelectList(_context.Storages, "Id", "Id", productIsAvalible.StorageId);
            return View(productIsAvalible);
        }

        // GET: ProductIsAvalibles/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var productIsAvalible = await _context.ProductIsAvalibles.FindAsync(id);
            if (productIsAvalible == null)
            {
                return NotFound();
            }
            ViewData["ProductId"] = new SelectList(_context.Products, "Id", "Id", productIsAvalible.ProductId);
            ViewData["StorageId"] = new SelectList(_context.Storages, "Id", "Id", productIsAvalible.StorageId);
            return View(productIsAvalible);
        }

        // POST: ProductIsAvalibles/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,ProductId,StorageId,Count")] ProductIsAvalible productIsAvalible)
        {
            if (id != productIsAvalible.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(productIsAvalible);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ProductIsAvalibleExists(productIsAvalible.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ProductId"] = new SelectList(_context.Products, "Id", "Id", productIsAvalible.ProductId);
            ViewData["StorageId"] = new SelectList(_context.Storages, "Id", "Id", productIsAvalible.StorageId);
            return View(productIsAvalible);
        }

        // GET: ProductIsAvalibles/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var productIsAvalible = await _context.ProductIsAvalibles
                .Include(p => p.Product)
                .Include(p => p.Storage)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (productIsAvalible == null)
            {
                return NotFound();
            }

            return View(productIsAvalible);
        }

        // POST: ProductIsAvalibles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var productIsAvalible = await _context.ProductIsAvalibles.FindAsync(id);
            _context.ProductIsAvalibles.Remove(productIsAvalible);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ProductIsAvalibleExists(int id)
        {
            return _context.ProductIsAvalibles.Any(e => e.Id == id);
        }
    }
}
